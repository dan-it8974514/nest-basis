import {
  MiddlewareConsumer,
  Module,
  NestModule,
  RequestMethod,
} from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { User } from '../../models/user.model';
import { UserModule } from '../user/user.module';
import { UserController } from '../user/user.controller';
import { UserService } from '../user/user.service';
import { AuthModule } from '../auth/auth.module';
import { LoggerMiddleware } from '../../middlewares/logger/logger.middleware';

@Module({
  imports: [
    SequelizeModule.forRoot({
      dialect: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'root',
      password: '00000000',
      database: 'nest',
      autoLoadModels: true,
      synchronize: true,
    }),
    SequelizeModule.forFeature([User]),
    UserModule,
    AuthModule,
  ],
  controllers: [AppController, UserController],
  providers: [AppService, UserService],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    console.log('Configure...');

    consumer.apply(LoggerMiddleware).forRoutes('user', 'auth');
  }
}
