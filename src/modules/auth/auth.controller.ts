import { Param, Controller, Post } from '@nestjs/common';
import { AuthService } from './auth.service';
import { EmailValidPipe } from '../../pipe/email-valid/email-valid.pipe';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('login/:email')
  login(@Param('email', new EmailValidPipe()) email: any) {
    return this.authService.login({
      email,
    });
  }
}
