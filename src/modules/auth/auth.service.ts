import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { User } from '../../models/user.model';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {
  constructor(
    @InjectModel(User)
    private readonly userModel: typeof User,
    private readonly jwtService: JwtService,
  ) {}

  async login(singInData: any) {
    console.log('singInData', singInData);

    const userData = await this.userModel.findOne({
      where: {
        email: singInData.email,
      },
    });

    if (!userData) {
      throw new NotFoundException('User not found');
    }

    const payload = {
      username: userData.dataValues.username,
      sub: userData.dataValues.id,
    };

    return {
      access_token: await this.jwtService.signAsync(payload),
    };
  }
}
