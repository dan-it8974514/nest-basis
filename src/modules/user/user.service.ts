import { Injectable } from '@nestjs/common';
import { User } from '../../models/user.model';
import { InjectModel } from '@nestjs/sequelize';

@Injectable()
export class UserService {
  constructor(@InjectModel(User) private userModel: typeof User) {}

  async addUser(userData: User): Promise<User> {
    return this.userModel.create(userData);
  }

  async getUser(id: number): Promise<User> {
    const user = await this.userModel.findByPk(id);

    if (!user) {
      throw new Error('User not found');
    }

    return user;
  }

  async getAllUsers({
    limit = 10,
    offset = 0,
  }: {
    limit: number;
    offset: number;
  }): Promise<User[]> {
    return this.userModel.findAll({
      limit,
      offset,
    });
  }
}
