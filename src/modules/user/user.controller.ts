import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import { User } from '../../models/user.model';
import { UserService } from './user.service';
import { AuthGuard } from '../auth/auth.guard';

@Controller('user')
export class UserController {
  constructor(private userService: UserService) {}

  @Get('/')
  @UseGuards(AuthGuard)
  getAllUsers(
    @Query()
    query = {
      limit: 10,
      offset: 0,
    },
  ): Promise<User[]> {
    return this.userService.getAllUsers({
      limit: Number(query.limit),
      offset: Number(query.offset),
    });
  }

  @Post('/')
  addUser(@Body() userData: User): Promise<User> {
    return this.userService.addUser(userData);
  }

  @Get('/:id')
  @UseGuards(AuthGuard)
  getUser(@Param('id') id: number): Promise<User> {
    return this.userService.getUser(id);
  }
}
