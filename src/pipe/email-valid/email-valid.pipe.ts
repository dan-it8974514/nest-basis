import { Injectable, PipeTransform } from '@nestjs/common';

@Injectable()
export class EmailValidPipe implements PipeTransform {
  transform(value: any) {
    const email = value;

    if (email && !/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)) {
      throw new Error('Email is not valid');
    }

    return value;
  }
}
